﻿﻿﻿# Repo initial pour VSCODE et SMING
Le repo initial contient le ficher de commande pour démarrer le conteneur du développement d'un projet ESP8266 avec le framework SMING.

Le SKD 1.5.4 et le framework SMING-3.0.1 sont utilisés.
Makefile-project.mk a été modifié pour télécharger code et SPIFFS.

Les autres repos sont concus pour être importés dans VSCODE à l'aide de git clone nomDuRepo.

Consulter la chaine vidéo associé ce type de développement.
https://www.youtube.com/watch?v=Z2K7bgcvJds&list=PLj02I15LkRHxa8lvvCD5mmaKN87pLDURb
